from django.contrib import admin

from .models import (
    Routine,
    Exercise,
    ExerciseSet,
    SessionLog,
    SessionEvent
)


class ExerciseSetInline(admin.TabularInline):
    model = Routine.esets.through


@admin.register(Routine)
class RoutineAdmin(admin.ModelAdmin):
    inlines = [
        ExerciseSetInline,
    ]


@admin.register(Exercise)
class ExerciseAdmin(admin.ModelAdmin):
    pass


@admin.register(ExerciseSet)
class ExerciseSetAdmin(admin.ModelAdmin):
    pass


class SessionEventInline(admin.TabularInline):
    model = SessionEvent


@admin.register(SessionLog)
class SessionLogAdmin(admin.ModelAdmin):
    inlines = [SessionEventInline, ]

import json

from django.core import serializers
from django.contrib.auth import get_user_model
from django.shortcuts import render
from django.urls import reverse
from django.views.generic import ListView
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _

from .models import SessionLog

User = get_user_model()


class DashboardListView(ListView):

	model = SessionLog

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)

		sessions = self.get_queryset()
		sessions_json = serializers.serialize("json", sessions)
		
		context["items"] = sessions
		context["items_json"] = sessions_json

		return context

dashboard_view = DashboardListView.as_view()
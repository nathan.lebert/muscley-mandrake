from django.db import models
from django.utils.translation import gettext as _

from django.contrib.auth import get_user_model

User = get_user_model()


class Exercise(models.Model):
    """
    Description: Tracks different types of exercises
    """

    EXERCISES = (("aero", _("Aerobic")), ("aner", _("Anerobic")))
    TARGETS = (("uppe", _("Upper")), ("lowe", _("Lower")))
    MOTIONS = (("push", _("Push")), ("pull", _("Pull")), ("card", _("Cardio")))

    exercise_type = models.CharField(choices=EXERCISES, max_length=4)
    motion_type = models.CharField(choices=MOTIONS, max_length=4)
    name = models.CharField(max_length=256)
    description = models.CharField(max_length=256, blank=True)
    target = models.CharField(choices=TARGETS, max_length=4)

    class Meta:
        pass

    def __str__(self):
        return self.name


class Routine(models.Model):
    """
    Description: Routines collect sets of specific exercises
    """

    name = models.CharField(max_length=256)
    description = models.CharField(max_length=256, blank=True)

    class Meta:
        pass

    def __str__(self):
        return self.name


class ExerciseSet(models.Model):
    """
    Description: Proxy model to track sets of exercise
    """

    exercise = models.ForeignKey(
        Exercise,
        on_delete=models.CASCADE,
        related_name="esets",
        related_query_name="eset",
    )

    routine = models.ManyToManyField(
        Routine,
        related_name="esets",
        related_query_name="eset",
    )
    repetitions = models.PositiveIntegerField(null=True, blank=True)
    duration = models.PositiveIntegerField(null=True, blank=True)

    class Meta:
        pass

    def __str__(self):
        if self.repetitions and self.repetitions > 0:
            return f"{self.repetitions}x {self.exercise.name}"

        return f"{self.duration}(s) {self.exercise.name}"


class SessionLog(models.Model):
    """
    Description: Tracks actual work out sessions, including reps (actual), weight,
    etc.
    """

    routine = models.ForeignKey(
        Routine,
        on_delete=models.SET_NULL,
        related_name="sessions",
        related_query_name="session",
        null=True
    )

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="sessions",
        related_query_name="session"
    )

    started = models.DateTimeField(auto_now_add=True)
    ended = models.DateTimeField(blank=True, null=True)
    weight = models.PositiveIntegerField(blank=True, null=True)

    # TODO: Auto add Session Event Logs for each exercise in routine on creation.

    class Meta:
        pass

    def __str__(self):
        return f"{self.user} - {self.routine.name} ({self.started})"


class SessionEvent(models.Model):
    """
    Description: Tracks performance on Exercise Sets during a Session.
    """

    session = models.ForeignKey(SessionLog, on_delete=models.CASCADE, related_name="events", related_query_name="event")
    exercise = models.ForeignKey(ExerciseSet, on_delete=models.SET_NULL, blank=True, null=True, related_name="events", related_query_name="event")
    repetitions_actual = models.PositiveIntegerField(blank=True, null=True)
    duration_actual = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        pass

    def __str__(self):
        return f"{self.session.user} - {self.session.routine} - {self.exercise.exercise.name}"

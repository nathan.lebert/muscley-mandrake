from django.test import TestCase
from django.urls import resolve
from fitness.views import dashboard_view


class FitnessDashboardTest(TestCase):

	def test_dashboard_url_resolves_to_dashboard_view(self):
		
		found = resolve('/fitness/')
		self.assertEqual(found.func, dashboard_view)
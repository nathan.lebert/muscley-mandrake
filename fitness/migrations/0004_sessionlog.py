# Generated by Django 2.2.9 on 2019-12-31 19:56

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('fitness', '0003_auto_20191231_1846'),
    ]

    operations = [
        migrations.CreateModel(
            name='SessionLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('started', models.DateTimeField(auto_now_add=True)),
                ('ended', models.DateTimeField()),
                ('routine', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='sessions', related_query_name='session', to='fitness.Routine')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sessions', related_query_name='session', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]

from selenium import webdriver
import unittest


class NewVisitorTest(unittest.TestCase):

	def setUp(self):
		self.browser = webdriver.Firefox()

	def tearDown(self):
		self.browser.quit()

	def test_can_see_homepage(self):

		# USER has heard about a cool new fitness app. 
		# They go to check out it's homepage
		self.browser.get('http://localhost:8000')

		# USER notices the page title and header mention Muscles
		assert 'Muscley Mandrake' in self.browser.title

		# User is invited to create an account

		# User registers a new account and is invited to start logging fitness data

if __name__ == "__main__":
	unittest.main(warnings='ignore')